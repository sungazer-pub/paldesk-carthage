//
//  ContentView.swift
//  paldesk_carthage
//
//  Created by Saverio Murgia on 09/01/2020.
//  Copyright © 2020 Saverio Murgia. All rights reserved.
//

import SwiftUI
import Paldesk

struct ContentView: View {
    var body: some View {
        Button(action: {
            print("Tapped")
        },
        label: {
            Text("Open Chat")
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
